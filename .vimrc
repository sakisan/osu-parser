nnoremap <leader>at :w<cr>:silent ! tmux send-keys -t 0 C-c Enter "cabal test --test-show-details=direct" Enter<cr><c-l>
nnoremap <leader>aa :w<cr>:silent ! tmux send-keys -t 0 C-c Enter "cabal run :analyze" Enter<cr><c-l>
nnoremap <leader>ah :w<cr>:silent ! tmux send-keys -t 0 C-c Enter "hlint %" Enter<cr><c-l>
