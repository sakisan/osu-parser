module OsuFile
  ( HitObject (..)
  , OsuFile (..)
  , General (..)
  , Editor (..)
  , Metadata (..)
  , Difficulty (..)
  , Events (..)
  , TimingPoints (..)
  , Colours (..)
  , HitObjects (..)
  , Parser
  , parseOsuFile
  , osuFileParser
  , analyzeParser
  ) where

import           Control.Monad              (void)
import           Data.Char
import           Data.List
import           Data.Maybe
import           Data.Text                  (Text)
import qualified Data.Text                  as Text
import           Data.Void
import           Text.Megaparsec
import           Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

data OsuFile = OsuFile
  { formatVersion :: Int
  , general       :: General
  , editor        :: Maybe Editor
  , metadata      :: Metadata
  , difficulty    :: Difficulty
  , events        :: Events
  , timingPoints  :: TimingPoints
  , colours       :: Maybe Colours
  , hitObjects    :: HitObjects
  }
  deriving (Show)

data General = General
  { audioFilename            :: String
  , spotifyUrl               :: Maybe String
  , audioLeadIn              :: Int
  , audioHash                :: Maybe String
  , previewTime              :: Int
  , countdown                :: Int
  , sampleSet                :: String
  , stackLeniency            :: Float
  , mode                     :: Int
  , letterboxInBreaks        :: Bool
  , storyFireInFront         :: Maybe Bool
  , useSkinSprites           :: Bool
  , alwaysShowPlayfield      :: Maybe Bool
  , overlayPosition          :: String
  , skinPreference           :: Maybe String
  , epilepsyWarning          :: Bool
  , countdownOffset          :: Int
  , specialStyle             :: Bool
  , widescreenStoryboard     :: Bool
  , samplesMatchPlaybackRate :: Bool
  , customSamples            :: Maybe Bool
  , editorBookmarks          :: Maybe [Int]
  , editorDistanceSpacing    :: Maybe Float
  }
  deriving (Show)

data Editor = Editor 
  { bookmarks       :: Maybe [Int]
  , distanceSpacing :: Maybe Float
  , beatDivisor     :: Maybe Float
  , gridSize        :: Maybe Int
  , timelineZoom    :: Maybe Float
  , currentTime     :: Maybe Int
  }
  deriving (Show)

data Metadata = Metadata
  { title         :: String
  , titleUnicode  :: Maybe String
  , artist        :: String
  , artistUnicode :: Maybe String
  , creator       :: String
  , version       :: String
  , source        :: Maybe String
  , tags          :: [String]
  , beatmapID     :: Maybe Int
  , beatmapSetID  :: Maybe Int
  }
  deriving (Show)

data Difficulty = Difficulty
  { hPDrainRate       :: Float
  , circleSize        :: Float
  , overallDifficulty :: Float
  , approachRate      :: Maybe Float
  , sliderMultiplier  :: Float
  , sliderTickRate    :: Float
  }
  deriving (Show)

data Events = Events
  {
  }
  deriving (Show)

data TimingPoints = TimingPoints
  {
  }
  deriving (Show)

data Colours = Colours
  { combo                 :: [(Int,String)]
  , sliderBorder          :: Maybe String
  , sliderTrackOverride   :: Maybe String
  , spinnerApproachCircle :: Maybe String
  , starBreakAdditive     :: Maybe String
  }
  deriving (Show)

data HitObjects = HitObjects
  {
  }
  deriving (Show)

data HitObject
  = Circle
  | Slider
  | Spinner
  deriving (Eq, Show)

parseOsuFile :: Text -> Either (ParseErrorBundle Text Void) OsuFile
parseOsuFile = runParser osuFileParser "" 

type Parser = Parsec Void Text

osuFileParser :: Parser OsuFile
osuFileParser = do
  space
  string "osu file format v"
  formatVersion <- read <$> some digitChar
  space
  general     <-             section "[General]"       generalParser
  colours1    <- optional $  section "[Colours]"       coloursParser
  editor      <- optional $  section "[Editor]"         editorParser
  metadata    <-             section "[Metadata]"     metadataParser
  difficulty1 <-             section "[Difficulty]" difficultyParser
  _           <-             section "[Events]"          sectionList
  _           <- optional $  section "[Difficulty]" difficultyParser
  option () $                section "[Events]"          sectionList
  _           <-             section "[TimingPoints]"    sectionList
  colours2    <- optional $  section "[Colours]"       coloursParser
  _           <-             section "[HitObjects]"      sectionList
  return $
    OsuFile
      { formatVersion = formatVersion
      , general       = general
      , editor        = editor
      , metadata      = metadata
      , difficulty    = difficulty1
      , events        = Events {}
      , timingPoints  = TimingPoints {}
      , colours       = colours1 <|> colours2
      , hitObjects    = HitObjects {}
      }

generalParser :: Parser General
generalParser =
  -- can appear in 2 places: 
  -- CountdownOffset, EpilepsyWarning, WidescreenStoryboard, StoryFireInFront,
  -- UseSkinSprites
  aggregator
    <$> optional          ( key "StoryFireInFront"           boolParser )
    <*>                     key "AudioFilename"            stringParser
    <*> optional          ( key "SpotifyURI"               stringParser )
    <*> option 0          ( key "AudioLeadIn"                 intParser )
    <*> optional          ( key "AudioHash"                stringParser )
    <*> option (-1)       ( key "PreviewTime"                 intParser )
    <*> option 1          ( key "Countdown"                   intParser )
    <*> optional          ( key "CountdownOffset"             intParser )
    <*> option "Normal"   ( key "SampleSet"                stringParser )
    <*> option 0.7        ( key "StackLeniency"             floatParser )
    <*> option 0          ( key "Mode"                        intParser )
    <*> option False      ( key "LetterboxInBreaks"          boolParser )
    <*> optional          ( key "WidescreenStoryboard"       boolParser )
    <*> optional          ( key "EpilepsyWarning"            boolParser )
    <*> optional          ( key "StoryFireInFront"           boolParser )
    <*> optional          ( key "UseSkinSprites"             boolParser )
    <*> optional          ( key "AlwaysShowPlayfield"        boolParser )
    <*> option "NoChange" ( key "OverlayPosition"          stringParser )
    <*> optional          ( key "SkinPreference"           stringParser )
    <*> optional          ( key "EpilepsyWarning"            boolParser )
    <*> optional          ( key "CountdownOffset"             intParser )
    <*> option False      ( key "SpecialStyle"               boolParser )
    <*> optional          ( key "WidescreenStoryboard"       boolParser )
    <*> option False      ( key "SamplesMatchPlaybackRate"   boolParser )
    <*> optional          ( key "CustomSamples"              boolParser )
    <*> optional          ( key "EditorBookmarks"            intsParser )
    <*> optional          ( key "EditorDistanceSpacing"     floatParser )
    <*> optional          ( key "UseSkinSprites"             boolParser )
  where
    aggregator storyFireInFront1 audioFilename spotifyUrl audioLeadIn audioHash previewTime countdown countdownOffset1 sampleSet stackLeniency mode letterboxInBreaks widescreenStoryboard1 epilepsyWarning1 storyFireInFront2 useSkinSprites1 alwaysShowPlayfield overlayPosition skinPreference epilepsyWarning2 countdownOffset2 specialStyle widescreenStoryboard2 samplesMatchPlaybackRate customSamples editorBookmarks editorDistanceSpacing useSkinSprites2 =
      General
        audioFilename
        spotifyUrl
        audioLeadIn
        audioHash
        previewTime
        countdown
        sampleSet
        stackLeniency
        mode
        letterboxInBreaks
        (storyFireInFront1 <|> storyFireInFront2)
        (fromMaybe False (useSkinSprites1 <|> useSkinSprites2))
        alwaysShowPlayfield
        overlayPosition
        skinPreference
        (fromMaybe False (epilepsyWarning1 <|> epilepsyWarning2))
        (fromMaybe 0 (countdownOffset1 <|> countdownOffset2))
        specialStyle
        (fromMaybe False (widescreenStoryboard1 <|> widescreenStoryboard2))
        samplesMatchPlaybackRate
        customSamples
        editorBookmarks
        editorDistanceSpacing

editorParser :: Parser Editor
editorParser =
  Editor
    <$> optional ( key "Bookmarks"        intsParser )
    <*> optional ( key "DistanceSpacing" floatParser )
    <*> optional ( key "BeatDivisor"     floatParser )
    <*> optional ( key "GridSize"          intParser )
    <*> optional ( key "TimelineZoom"    floatParser )
    <*> optional ( key "CurrentTime"       intParser )


metadataParser :: Parser Metadata
metadataParser = 
  Metadata 
    <$>            key "Title"         stringParser
    <*> optional ( key "TitleUnicode"  stringParser )
    <*>            key "Artist"        stringParser
    <*> optional ( key "ArtistUnicode" stringParser )
    <*>            key "Creator"       stringParser
    <*>            key "Version"       stringParser
    <*> optional ( key "Source"        stringParser )
    <*> option []( key "Tags"            tagsParser )
    <*> optional ( key "BeatmapID"        intParser )
    <*> optional ( key "BeatmapSetID"     intParser )
  where 
    tagsParser = do
      tags <- lexeme stringsParser  
      optional $ lexeme (noneOf charsStrayLineCantStartWith >> sectionLine)
      return tags
    -- There's one map with a stray line after the tags
    -- This a semi-attempt at not completely hardcoding it
    charsStrayLineCantStartWith :: String
    charsStrayLineCantStartWith = "[B"

difficultyParser :: Parser Difficulty
difficultyParser = 
  Difficulty 
    <$>            key "HPDrainRate"       floatParser
    <*>            key "CircleSize"        floatParser
    <*>            key "OverallDifficulty" floatParser
    <*> optional ( key "ApproachRate"      floatParser )
    <*>            key "SliderMultiplier"  floatParser
    <*>            key "SliderTickRate"    floatParser

coloursParser :: Parser Colours
coloursParser = do
  sliderBorder1         <- optional $ key "SliderBorder"          stringParser
  combos                <- many $                           lexeme comboParser 
  _                     <- optional $ key "SliderBorderColor"     stringParser
  sliderBorder2         <- optional $ key "SliderBorder"          stringParser
  sliderTrackOverride   <- optional $ key "SliderTrackOverride"   stringParser
  sliderBorder3         <- optional $ key "SliderBorder"          stringParser
  spinnerApproachCircle <- optional $ key "SpinnerApproachCircle" stringParser
  starBreakAdditive     <- optional $ key "StarBreakAdditive"     stringParser
  sectionKeys -- ignoring CCombo, ShrekBestWaifu and whatever else
  return $
    Colours
      combos
      (sliderBorder1 <|> sliderBorder2 <|> sliderBorder3)
      sliderTrackOverride
      spinnerApproachCircle
      starBreakAdditive
      

comboParser :: Parser (Int, String)
comboParser = do 
  string "Combo" 
  combo <- intParser
  semicolon
  colour <- stringParser
  return (combo, colour)

semicolon :: Parser ()
semicolon = hspace >> char ':' >> hspace

key :: Text -> Parser a -> Parser a
key name p =
  string' name >> hspace >> char ':' >> hspace >> lexeme p
    <?> Text.unpack name

section :: Text -> Parser a -> Parser a
section name sectionParser =
  lexeme (string name) >> lexeme sectionParser

stringParser :: Parser String
stringParser = Text.unpack <$> takeWhileP Nothing (`notElem` nl)

nl :: String
nl = "\r\n"

lexeme :: Parser a -> Parser a
lexeme = L.lexeme (space <|> void (char '\r'))

boolParser :: Parser Bool
boolParser = False <$ char '0' <|> True <$ char '1'

intsParser :: Parser [Int]
intsParser = try (intParser `sepBy` ",") <|> intParser `sepBy` ", "

stringsParser :: Parser [String]
stringsParser = stringParser `sepBy` " "

floatParser :: Parser Float
floatParser =
  try (L.signed space L.float)
    <|> L.signed space L.decimal
    <|> (infinity <$ string "NaN")
  where
    infinity = encodeFloat (floatRadix 0 - 1) (snd $ floatRange 0)

intParser :: Parser Int
intParser = L.signed space L.decimal


-- ANALYZING

analyzeParser :: Parser [(Text, (Int, [Text]))]
analyzeParser = do
  space
  string "osu file format v"
  version <- read <$> some digitChar
  space
  general <- section "[General]" sectionKeys
  colours1 <- option [] $ section "[Colours]" sectionKeys
  editor <- option [] $ section "[Editor]" sectionKeys
  metadata <- section "[Metadata]" sectionKeys
  difficulty1 <- section "[Difficulty]" sectionKeys
  section "[Events]" sectionList
  difficulty2 <- option [] $ section "[Difficulty]" sectionKeys
  option () $ section "[Events]" sectionList
  section "[TimingPoints]" sectionList
  colours2 <- option [] $ section "[Colours]" sectionKeys
  section "[HitObjects]" sectionList
  return
    [ ("[General]", (version, general))
    , ("[Metadata]", (version, metadata))
    , ("[Editor]", (version, editor))
    , ( "[Difficulty]"
      , (version, difficulty1 ++ intersect difficulty1 difficulty2))
    , ("[Events]", (version, []))
    , ("[TimingPoints]", (version, []))
    , ("[Colours]", (version, colours1 ++ colours2))
    , ("[HitObjects]", (version, []))
    ]

sectionKeys :: Parser [Text]
sectionKeys =
  fmap catMaybes . many $
    choice
      [ try $ Just <$> keyName <* (semicolon >> lexeme stringParser)
      , Nothing <$ lexeme sectionLine
      ]
  where
    keyName = takeWhile1P Nothing isAlphaNum

sectionLine :: Parser Text 
sectionLine = 
    noneOf (nextSection ++ nl) >> takeWhile1P Nothing (`notElem` nl)
  where
    nextSection :: String
    nextSection = "["

sectionList :: Parser ()
sectionList =
  () <$ many (lexeme sectionLine)
