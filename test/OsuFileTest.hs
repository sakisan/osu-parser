module Main
  ( main,
  )
where

import           Data.Text             (Text)
import           Data.Text.IO          hiding (putStrLn)
import           OsuFile
import           Prelude               hiding (readFile)
import           Test.Tasty
import           Test.Tasty.HUnit
import           Text.Megaparsec

{-
 - https://github.com/feuerbach/tasty
 - https://hackage.haskell.org/package/tasty-1.4.0.1/docs/Test-Tasty.html
 - https://hackage.haskell.org/package/tasty-hunit-0.10.0.3/docs/Test-Tasty-HUnit.html
 -}
main :: IO ()
main = do
  rubiksCube <- readFile "test/data/114635.osu"
  defaultMain (tests rubiksCube)

tests :: Text -> TestTree
tests rubiksCube = rubiksCubeTests rubiksCube

rubiksCubeTests :: Text -> TestTree
rubiksCubeTests rubiksCube =
  testGroup "Rubik's Cube" $
  case parseOsuFile rubiksCube of
    Left bundle ->
      [testCase "parseOsuFile" . assertFailure $ errorBundlePretty bundle]
    Right osuFile ->
      [ testCase "version" $ do version osuFile @?= 9
      , testGroup "General" $ rubiksGeneralTests osuFile
      , testGroup "Editor" $ rubiksEditorTests $ editor osuFile
      ]

rubiksGeneralTests osuFile =
  [ testCase "AudioFilename" $
    audioFilename (general osuFile) @?= "Rubik's Cube.mp3"
  , testCase "AudioLeadIn" $ audioLeadIn (general osuFile) @?= 1500
  , testCase "PreviewTime" $ previewTime (general osuFile) @?= 67220
  , testCase "Countdown" $ countdown (general osuFile) @?= 0
  , testCase "SampleSet" $ sampleSet (general osuFile) @?= "Soft"
  , testCase "StackLeniency" $ stackLeniency (general osuFile) @?= 0.7
  , testCase "Mode" $ mode (general osuFile) @?= 0
  , testCase "LetterboxInBreaks" $ letterboxInBreaks (general osuFile) @?= True
  , testCase "EditorBookmarks" $ editorBookmarks (general osuFile) @?= Nothing
  ]

rubiksEditorTests :: Maybe Editor -> [TestTree]
rubiksEditorTests Nothing =
  [testCase "" $ assertFailure "Editor section missing"]
rubiksEditorTests (Just section) =
  [ testCase "DistanceSpacing" $ distanceSpacing section @?= Just 1
  , testCase "BeatDivisor" $ beatDivisor section @?= Just 4
  , testCase "GridSize" $ gridSize section @?= Just 4
  ]
