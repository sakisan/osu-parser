module Main (main) where

import           Control.Monad.Loops
import           Data.List
import           Data.Maybe
import           Data.Text           (Text)
import qualified Data.Text           as Text
import           Data.Text.IO        hiding (putStrLn)
import           Debug.Trace
import           OsuFile
import           Prelude             hiding (readFile)
import           System.Directory
import           System.IO           hiding (hGetContents, readFile)
import           Text.Megaparsec
import           Text.Printf

main :: IO ()
main = do
  -- parseFile osuFileParser "114635.osu"
  -- analyzeFiles
  osuParseAllFiles
  return ()

osuParseAllFiles :: IO ()
osuParseAllFiles = do
  allOsuFiles <-
    delete ".." . delete "." <$>
    getDirectoryContents "test/data/2020_12_01_osu_files/"
  putStrLn $ "Parsing all " ++ show (length allOsuFiles) ++ " files now"
  processed <- unfoldrM (parseFile osuFileParser) allOsuFiles
  putStrLn $ "Parsed " ++ show (length processed) ++ " files\n"
  return ()

analyzeFiles :: IO ()
analyzeFiles = do
  allOsuFiles <-
    delete ".." . delete "." <$>
    getDirectoryContents "test/data/2020_12_01_osu_files/"
  putStrLn $ "Parsing all " ++ show (length allOsuFiles) ++ " files now"
  processed <- unfoldrM (parseFile analyzeParser) allOsuFiles
  putStrLn $ "Parsed " ++ show (length processed) ++ " files\n"
  putStrLn $ report processed
  return ()


parseFile :: Parser a -> [String] -> IO (Maybe (a, [String]))
parseFile _ [] = pure Nothing
parseFile parser (filePath:more) = do
  putStrLn $ "Parsing " ++ filePath
  -- todo use Data.ByteString.readFile and handle character encoding yourself
  -- (for better speed and robustness)
  withFile ("test/data/2020_12_01_osu_files/" ++ filePath) ReadMode $ \handle -> do
    hSetEncoding handle (encodingFor filePath)
    file <- hGetContents handle
    case runParser parser "" file of
      Left bundle -> do
        putStrLn $ errorBundlePretty bundle
        return Nothing
      Right x -> return (Just (x, more))
  where
    encodingFor f
      | f == "191276.osu" = utf16
      | f == "49374.osu" = latin1
      | otherwise = utf8_bom

report :: [[(Text, (Int, [Text]))]] -> String
report parsed =
  unlines
    [ helper "[General]" generalProperties
    , helper "[Editor]" editorProperties
    , helper "[Metadata]" metadataProperties
    , helper "[Difficulty]" difficultyProperties
    , helper "[Colours]" coloursProperties
    ]
  where
    section name = map snd . filter ((name ==) . fst) $ concat parsed
    helper name properties = sectionReport name properties (section name)
    

sectionReport :: Text -> [Text] -> [(Int, [Text])] -> String
sectionReport heading properties entries =
  unlines $
  format heading total allVersions :
  [ format f count versions
  | f <- properties
  , let Just (count, versions) = lookup f database
  ] ++
  [ "difference in order in " ++ show orderCount ++ " files"
  , "unregistered fields: " ++ show unregistered
  ]
  where
    format :: Text -> Int -> [Int] -> String
    format field count versions =
      concat
        [ printf "%-26s" $ Text.unpack field
        , printf "%-7d" count
        , "[" ++ intercalate "," (showVersion versions <$> allVersions) ++ "]"
        ]
    showVersion versions v
      | v `elem` versions = show v
      | otherwise = show v >> " "
    allVersions = sort . nub . concatMap (snd . snd) $ database
    total = length $ filter (not . null . snd) entries
    database :: [(Text, (Int, [Int]))]
    orderCount :: Int
    unregistered :: [Text]
    (database, (orderCount, unregistered)) =
      foldr processEntry (zip properties (repeat (0, [])), (0, [])) entries
    processEntry (version, fields) (acc, orderCount) =
      ( foldr (increment version) acc fields
      , checkOrder version fields orderCount)
    increment version field acc =
      [ ( f
        , if f == field
            then (count + 1, nub (version : versions))
            else (count, versions))
      | (f, (count, versions)) <- acc
      ]
    checkOrder version fields (count, unregistered) =
      (count + oneOrZero, nub $ unregistered ++ unregistered')
      where
        indexes = mapMaybe (`elemIndex` properties) fields
        oneOrZero
          | and ((zipWith (<) <*> tail) indexes) = 0
          | otherwise = {-traceShow fields-} 1
        unregistered' = filter (`notElem` properties) fields

generalProperties :: [Text]
generalProperties =
  [ "AudioFilename"
  , "SpotifyURI"
  , "AudioLeadIn"
  , "AudioHash"
  , "PreviewTime"
  , "Countdown"
  , "SampleSet"
  , "StackLeniency"
  , "Mode"
  , "LetterboxInBreaks"
  , "StoryFireInFront"
  , "UseSkinSprites"
  , "AlwaysShowPlayfield"
  , "OverlayPosition"
  , "SkinPreference"
  , "EpilepsyWarning"
  , "CountdownOffset"
  , "SpecialStyle"
  , "WidescreenStoryboard"
  , "SamplesMatchPlaybackRate"
  , "CustomSamples"
  , "EditorBookmarks"
  , "EditorDistanceSpacing"
  ]

editorProperties :: [Text]
editorProperties = 
  [ "Bookmarks"
  , "DistanceSpacing"
  , "BeatDivisor"
  , "GridSize"
  , "TimelineZoom"
  , "CurrentTime"
  ]

metadataProperties :: [Text]
metadataProperties = 
  [ "Title"
  , "TitleUnicode"
  , "Artist"
  , "ArtistUnicode"
  , "Creator"
  , "Version"
  , "Source"
  , "Tags"
  , "BeatmapID"
  , "BeatmapSetID"
  ]

difficultyProperties :: [Text]
difficultyProperties =
  [ "HPDrainRate"
  , "CircleSize"
  , "OverallDifficulty"
  , "ApproachRate"
  , "SliderMultiplier"
  , "SliderTickRate"
  ]

coloursProperties :: [Text]
coloursProperties =
  [ "Combo1"
  , "Combo2"
  , "Combo3"
  , "Combo4"
  , "Combo5"
  , "Combo6"
  , "Combo7"
  , "Combo8"
  , "Combo12"
  , "Combo34"
  , "Combo56"
  , "Combo78"
  , "Combo90"
  , "CCombo1"
  , "SliderBorder"
  , "sliderborder"
  , "Sliderborder"
  , "sliderBorder"
  , "SilderBorder"
  , "SliderBorderColor"
  , "SliderBoarder"
  , "SliderBody"
  , "SliderTrackOverride"
  , "SpinnerApproachCircle"
  , "StarBreakAdditive"
  , "ShrekBestWaifu"
  ]

